FROM python:3.7-buster

COPY requirements.txt requirements.txt

USER root

RUN pip install -r requirements.txt

USER gitpod