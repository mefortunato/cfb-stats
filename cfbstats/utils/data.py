import re
import hashlib
import requests
import pandas as pd

SCHEDULE_URL = 'https://www.sports-reference.com/cfb/years/{}-schedule.html'
GAME_URL = 'https://www.sports-reference.com/cfb/boxscores/{}.html'

def get_games(year: str) -> list:
    resp = requests.get(SCHEDULE_URL.format(year))
    html = resp.text
    game_links = re.findall(r'/cfb/boxscores/(.*?).html', html)
    tables = pd.read_html(html)
    assert len(tables) == 1
    games = tables[0]
    reformat_games(games)
    games['GAME_LINK'] = game_links
    return games

def hash_row(row):
    return hashlib.sha1(row.values.tobytes()).hexdigest()

def reformat_games(df: pd.DataFrame) -> pd.DataFrame:
    del df['Rk']
    del df['Day']
    df.drop(df.loc[df['Wk']=='Wk'].index, inplace=True)
    df.reset_index(drop=True)
    df['DATE'] = pd.to_datetime(df['Date'])
    df['TIME'] = pd.to_datetime(df['Time']).dt.time
    winner_away = df['Unnamed: 7']=='@'
    df.loc[winner_away, 'AWAY'] = df.loc[winner_away, 'Winner']
    df.loc[winner_away, 'AWAY_PTS'] = df.loc[winner_away, 'Pts']
    df.loc[winner_away, 'HOME'] = df.loc[winner_away, 'Loser']
    df.loc[winner_away, 'HOME_PTS'] = df.loc[winner_away, 'Pts.1']
    df.loc[~winner_away, 'AWAY'] = df.loc[~winner_away, 'Loser']
    df.loc[~winner_away, 'AWAY_PTS'] = df.loc[~winner_away, 'Pts.1']
    df.loc[~winner_away, 'HOME'] = df.loc[~winner_away, 'Winner']
    df.loc[~winner_away, 'HOME_PTS'] = df.loc[~winner_away, 'Pts']
    df['HOME_RANK'] = df['HOME'].str.extract(r"\((.*)\)")
    df['AWAY_RANK'] = df['AWAY'].str.extract(r"\((.*)\)")
    df['HOME'] = df['HOME'].str.replace(r"\(.*\)","")
    df['AWAY'] = df['AWAY'].str.replace(r"\(.*\)","")
    df['BOWL'] = df['Notes'].str.contains('Bowl')
    df.loc[df['Notes'].str.contains('Championship'), 'BOWL'] = True
    df.rename(columns={'Wk': 'WEEK'}, inplace=True)
    df['WINNER'] = df['HOME_PTS'] > df['AWAY_PTS']
    COLUMNS_TO_KEEP = [
        'WEEK', 'DATE', 'TIME', 'HOME', 'AWAY', 'HOME_PTS', 'AWAY_PTS',
        'HOME_RANK', 'AWAY_RANK', 'BOWL', 'WINNER'
    ]
    COLUMNS_TO_DROP = df.columns[~df.columns.isin(COLUMNS_TO_KEEP)]
    df.drop(columns=COLUMNS_TO_DROP, inplace=True)
    df['_id'] = df.apply(hash_row, axis=1)
    df.set_index('_id', inplace=True)

def get_stats_from_game_link(game_id: str, game_link: str) -> pd.DataFrame:
    resp = requests.get(GAME_URL.format(game_link))
    html = resp.text
    html = html.replace('<!--', '')
    html = html.replace('-->', '')
    game_stats = pd.read_html(html)
    
    (
        quarters, scoring_plays, 
        summary, passing, rush_recv, 
        defense, kick_return, kicking
    ) = game_stats[-8:]
    if passing.droplevel(0, 1)['School'].unique().size == 1:
        return pd.DataFrame()
    quarters = quarter_stats(game_id, quarters)
    summary = summary_stats(game_id, summary)
    defense = defense_stats(game_id, defense)
    kicking = kicking_stats(game_id, kicking)
    return pd.concat([quarters, summary, defense, kicking], axis=1)

def kicking_stats(game_id: str, df: pd.DataFrame) -> pd.DataFrame:
    df = df.droplevel(0, 1)
    df = df[(df['Player']!='Player') & (~df['Player'].isna())]
    away_name = df['School'].iloc[0]
    home_name = df['School'].iloc[-1]
    df['XPM'] = df['XPM'].astype(float)
    df['XPA'] = df['XPA'].astype(float)
    df['FGM'] = df['FGM'].astype(float)
    df['FGA'] = df['FGA'].astype(float)
    team_sum = df.groupby('School').sum().rename(index={away_name: 0, home_name: 1})
    team_sum['GAME_ID'] = game_id
    team_sum.reset_index(inplace=True)
    team_sum.rename(columns={'School': 'HOME'}, inplace=True)
    team_sum = team_sum.sort_values('HOME').reset_index(drop=True)
    team_sum.loc[[1, 0], 'OPP_XPM'] = team_sum.loc[[0, 1], 'XPM'].values
    team_sum.loc[[1, 0], 'OPP_XPA'] = team_sum.loc[[0, 1], 'XPA'].values
    team_sum.loc[[1, 0], 'OPP_FGM'] = team_sum.loc[[0, 1], 'FGM'].values
    team_sum.loc[[1, 0], 'OPP_FGA'] = team_sum.loc[[0, 1], 'FGA'].values
    return team_sum

def defense_stats(game_id: str, df: pd.DataFrame) -> pd.DataFrame:
    df = df.droplevel(0, 1)
    df = df[(df['Player']!='Player') & (~df['Player'].isna())]
    df['TK_LOSS'] = df['Loss'].astype(float)
    df['SACK'] = df['Sk'].astype(float)
    df['PD'] = df['PD'].astype(float)
    away_name = df['School'].iloc[0]
    home_name = df['School'].iloc[-1]
    team_sum = df.groupby('School').sum().rename(index={away_name: 0, home_name: 1})
    team_sum['GAME_ID'] = game_id
    team_sum.reset_index(inplace=True)
    team_sum.rename(columns={'School': 'HOME'}, inplace=True)
    team_sum = team_sum.sort_values('HOME').reset_index(drop=True)
    team_sum.loc[[1, 0], 'OPP_TK_LOSS'] = team_sum.loc[[0, 1], 'TK_LOSS'].values
    team_sum.loc[[1, 0], 'OPP_SACK'] = team_sum.loc[[0, 1], 'SACK'].values
    team_sum.loc[[1, 0], 'OPP_PD'] = team_sum.loc[[0, 1], 'PD'].values
    return team_sum

def summary_stats(game_id: str, df: pd.DataFrame) -> pd.DataFrame:
    df.set_index('Stat', inplace=True)
    stats = pd.DataFrame(index=[0, 1])
    stats.loc[[0, 1], 'GAME_ID'] = game_id
    stats.loc[[0, 1], 'HOME'] = [0, 1]
    stats['FD'] = df.loc['First Downs'].values
    stats.loc[[1, 0], 'OPP_FD'] = df.loc['First Downs'].values
    stats['TOV'] = df.loc['Turnovers'].values
    stats.loc[[1, 0], 'OPP_TOV'] = df.loc['Turnovers'].values
    penalties = df.loc['Penalties-Yards'].str.split('-', expand=True)
    stats['PEN'] = penalties[0].values
    stats.loc[[1, 0], 'OPP_PEN'] = penalties[0].values
    stats['PEN_YDS'] = penalties[1].values
    stats.loc[[1, 0], 'PEN_YDS'] = penalties[1].values
    rushing = df.loc['Rush-Yds-TDs'].str.split('-', expand=True)
    stats['RUSH'] = rushing[0].values
    stats.loc[[1, 0], 'OPP_RUSH'] = rushing[0].values
    stats['RUSH_YDS'] = rushing[1].values
    stats.loc[[1, 0], 'OPP_RUSH_YDS'] = rushing[1].values
    stats['RUSH_TD'] = rushing[2].values
    stats.loc[[1, 0], 'OPP_RUSH_TD'] = rushing[2].values
    passing = df.loc['Cmp-Att-Yd-TD-INT'].str.split('-', expand=True)
    stats['PASS_CMP'] = passing[0].values
    stats.loc[[1, 0], 'OPP_PASS_CMP'] = passing[0].values
    stats['PASS_ATT'] = passing[1].values
    stats.loc[[1, 0], 'OPP_PASS_ATT'] = passing[1].values
    stats['PASS_YDS'] = passing[2].values
    stats.loc[[1, 0], 'OPP_PASS_YDS'] = passing[2].values
    stats['PASS_TD'] = passing[3].values
    stats.loc[[1, 0], 'OPP_PASS_TD'] = passing[3].values
    stats['PASS_INT'] = passing[4].values
    stats.loc[[1, 0], 'OPP_PASS_INT'] = passing[4].values
    return stats

def quarter_stats(game_id: str, df: pd.DataFrame) -> pd.DataFrame:
    quarters = pd.DataFrame()
    q1 = df['1'][0] > df['1'][1]
    q2 = df['2'][0] > df['2'][1]
    q3 = df['3'][0] > df['3'][1]
    q4 = df['4'][0] > df['4'][1]
    quarters = quarters.append({
        'GAME_ID': game_id, 'Q1_WON': q1, 'Q2_WON': q2,
        'Q3_WON': q3, 'Q4_WON': q4, 'HOME': 0
    }, ignore_index=True)
    quarters = quarters.append({
        'GAME_ID': game_id, 'Q1_WON': ~q1, 'Q2_WON': ~q2,
        'Q3_WON': ~q3, 'Q4_WON': ~q4, 'HOME': 1
    }, ignore_index=True)
    return quarters

def get_game_stats(games: pd.DataFrame) -> pd.DataFrame:
    stats = pd.DataFrame()
